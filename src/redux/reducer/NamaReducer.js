const initialState = {};

export const NamaReducer = (state = initialState, action) => {
  if (action.type == 'SET_USER') {
    return action.payload;
  }
  return state;
};
