import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Card = ({ positif, sembuh, meninggal, provinsi }) => {
  return (
    <View style={styles.card}>
      <Text style={styles.namaProvinsi}>{provinsi}</Text>
      <View style={styles.wrappDataKasus}>
        <View>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#FDAC4D' }}>
            {positif}
          </Text>
          <View style={{ height: 8 }} />
          <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#3F3D56' }}>
            Positif
          </Text>
        </View>
        <View>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#7BFD4D' }}>
            {sembuh}
          </Text>
          <View style={{ height: 8 }} />
          <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#3F3D56' }}>
            Sembuh
          </Text>
        </View>
        <View>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#FD574D' }}>
            {meninggal}
          </Text>
          <View style={{ height: 8 }} />
          <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#3F3D56' }}>
            Meninggal
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#E6E6E6',
    marginHorizontal: 50,
    height: 97,
    borderRadius: 5,
    elevation: 4,
    marginBottom: 30,
  },
  wrappDataKasus: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  namaProvinsi: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#3F3D56',
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 15,
  },
});
