import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Padri from '../../assets/image/Padri.png';
import Fb from '../../assets/image/fb.png';
import Ig from '../../assets/image/ig.png';
import Tw from '../../assets/image/twit.png';
import Rosa from '../../assets/image/Silaban.png';
import {
  FontAwesome5,
  MaterialIcons,
  AntDesign,
  Entypo,
} from '@expo/vector-icons';

const Muhammad = ({
  onPressed,
  pressFb,
  pressTw,
  pressIg,
  pressFb1,
  pressTw1,
}) => {
  return (
    <View>
      <View style={styles.card}>
        <View style={styles.gambar}>
          <Image source={Padri} style={{ height: 100, width: 100 }} />
          <View>
            <Text style={styles.tulisan}> Muhammad Padri </Text>
            <View style={{ height: 20 }} />
            <View style={styles.logo}>
              <TouchableOpacity onPress={pressFb1}>
                <Entypo name="facebook" size={32} color="#0576e8" />
              </TouchableOpacity>
              <TouchableOpacity onPress={pressIg}>
                <Entypo name="instagram" size={32} color="#FF6584" />
              </TouchableOpacity>
              <TouchableOpacity onPress={pressTw1}>
                <Entypo name="twitter" size={32} color="#0558e8" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      {/* pressFb, onPressed , pressTw*/}
      <View style={styles.card}>
        <View style={styles.gambar}>
          <Image source={Rosa} style={{ height: 100, width: 100 }} />
          <View>
            <Text style={styles.tulisan}> Evi Rosalina Silaban </Text>
            <View style={{ height: 20 }} />
            <View style={styles.logo}>
              <TouchableOpacity onPress={pressFb}>
                <Entypo name="facebook" size={32} color="#0576e8" />
              </TouchableOpacity>
              <TouchableOpacity onPress={onPressed}>
                <Entypo name="instagram" size={32} color="#FF6584" />
              </TouchableOpacity>
              <TouchableOpacity onPress={pressTw}>
                <Entypo name="twitter" size={32} color="#0558e8" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Muhammad;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#E5E5E5',
    marginHorizontal: 20,
    height: 120,
    borderRadius: 15,
    elevation: 4,
    marginBottom: 60,
  },
  wrappDataKasus: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  gambar: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 5,
    justifyContent: 'space-around',
  },
  tulisan: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#3F3D56',
  },
  logo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
