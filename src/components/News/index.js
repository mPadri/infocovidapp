import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
// import Image1 from '../../assets/image/img1.png';

const News = ({ title, source, photo, onPressed }) => {
  return (
    <TouchableOpacity style={styles.wrappNews} onPress={onPressed}>
      <View>
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.titleNews}>
          {title}
        </Text>
        <Text style={styles.sumber}>{source}</Text>
      </View>
      <Image source={{ uri: photo }} style={{ width: 137, height: 75 }} />
    </TouchableOpacity>
  );
};

export default News;

const styles = StyleSheet.create({
  wrappNews: {
    flexDirection: 'row',
    marginHorizontal: 30,
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  titleNews: {
    width: 150,
    fontSize: 14,
    fontWeight: 'bold',
    color: '#3F3D56',
    marginBottom: 5,
  },
  sumber: {
    fontSize: 11,
    color: '#C4C4C4',
  },
});
