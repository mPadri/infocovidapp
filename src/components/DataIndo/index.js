import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const DataIndo = ({ positif, sembuh, meninggal }) => {
  return (
    <View style={styles.wrappDataKasus}>
      <View>
        <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#FDAC4D' }}>
          {positif}
        </Text>
        <View style={{ height: 20 }} />
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FDAC4D' }}>
          Positif
        </Text>
      </View>
      <View>
        <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#7BFD4D' }}>
          {sembuh}
        </Text>
        <View style={{ height: 20 }} />
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#7BFD4D' }}>
          Sembuh
        </Text>
      </View>
      <View>
        <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#FD574D' }}>
          {meninggal}
        </Text>
        <View style={{ height: 20 }} />
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FD574D' }}>
          Meninggal
        </Text>
      </View>
    </View>
  );
};

export default DataIndo;

const styles = StyleSheet.create({
  wrappDataKasus: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 20,
  },
  wrappNews: {
    flexDirection: 'row',
    marginHorizontal: 30,
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  titleNews: {
    width: 150,
    fontSize: 14,
    fontWeight: 'bold',
    color: '#3F3D56',
    marginBottom: 5,
  },
  sumber: {
    fontSize: 11,
    color: '#C4C4C4',
  },
});
