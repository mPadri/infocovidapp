import { FontAwesome5, FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import AboutUs from '../screens/AboutUs';
import Home from '../screens/Home';
import Login from '../screens/Login';
import Region from '../screens/Region';
import Tips from '../screens/Tips';
import DetailNews from '../screens/DetailNews';
import DetailSosmed from '../screens/DetailSosmed';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen
      name="Home"
      component={Home}
      options={{
        tabBarIcon: ({ color }) => (
          <FontAwesome name="home" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Region"
      component={Region}
      options={{
        tabBarIcon: ({ color }) => (
          <FontAwesome name="map" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Tips"
      component={Tips}
      options={{
        tabBarIcon: ({ color }) => (
          <FontAwesome5 name="hand-holding-heart" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="AboutUs"
      component={AboutUs}
      options={{
        tabBarIcon: ({ color }) => (
          <FontAwesome5 name="info-circle" color={color} size={26} />
        ),
      }}
    />
  </Tabs.Navigator>
);

const Route = () => {
  return (
    <Stack.Navigator headerMode="none" initialRouteName="Login">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="MainApp" component={TabsScreen} />
      <Stack.Screen name="DetailNews" component={DetailNews} />
      <Stack.Screen name="DetailSosmed" component={DetailSosmed} />
    </Stack.Navigator>
  );
};

export default Route;
