import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import Constant from 'expo-constants';

const DetailNews = ({ route }) => {
  const url = route.params.url;
  return (
    <WebView
      domStorageEnabled={true}
      javaScriptEnabled={true}
      source={{ uri: url }}
      style={{ marginTop: Constant.statusBarHeight }}
    />
  );
};

export default DetailNews;

const styles = StyleSheet.create({});
