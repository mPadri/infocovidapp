import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import Safety from '../../assets/image/Safety.png';
import Tissu from '../../assets/image/Tissu.png';
import Wash from '../../assets/image/Wash.png';
import Clean from '../../assets/image/Clean.png';
import Distance from '../../assets/image/Distance.png';
import Masker from '../../assets/image/Masker.png';

const Tips = () => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.Tip}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 30,
              textAlign: 'center',
              marginLeft: 10,
              color: '#3F3D56',
            }}
          >
            Safety Tips in {'\n'}COVID-19
          </Text>
          <Image
            source={Safety}
            style={{ width: 120, height: 120, marginLeft: 10 }}
          />
        </View>
        <Text style={styles.Tulisan}>
          Follow these five easy steps to help prevent the spread of COVID-19 :
        </Text>
        <View style={styles.Step}>
          <Image source={Tissu} style={{ width: 100, height: 100 }} />
          <Text style={styles.Step2}>
            Sneeze or cough? Cover your nose and mouth with a tissue or use your
            elbow.
          </Text>
        </View>
        <View style={styles.Step}>
          <Image source={Wash} style={{ width: 100, height: 100 }} />
          <Text style={styles.Step2}>
            Wash your hands often with soap and water for at least 20 seconds.
          </Text>
        </View>
        <View style={styles.Step}>
          <Image source={Clean} style={{ width: 100, height: 100 }} />
          <Text style={styles.Step2}>
            Clean and disinfect surfaces around your home and work frequently.
          </Text>
        </View>
        <View style={styles.Step}>
          <Image source={Distance} style={{ width: 100, height: 100 }} />
          <Text style={styles.Step2}>
            Keep at least 6 feet between yourself and others if you must be in
            public.
          </Text>
        </View>
        <View style={styles.Step}>
          <Image source={Masker} style={{ width: 100, height: 100 }} />
          <Text style={styles.Step2}>
            Wear a cloth face covering over your mouth and nos when around
            others.
          </Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default Tips;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  Tip: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 190,
  },
  Step: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  Step2: {
    fontSize: 17,
    fontWeight: 'bold',
    marginLeft: 30,
    textAlign: 'left',
    width: 200,
    color: '#3F3D56',
  },
  Tulisan: {
    fontSize: 17,
    fontWeight: 'bold',
    paddingHorizontal: 15,
    marginBottom: 15,
    color: '#3F3D56',
  },
});
