import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Constant from 'expo-constants';
import Image3 from '../../assets/image/img3.png';
import { FontAwesome5, MaterialIcons } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';

const Login = ({ navigation }) => {
  const [nama, setNama] = useState('');
  const [error, setError] = useState(false);
  // console.log(nama);
  const dispatch = useDispatch();

  const moveToHome = () => {
    if (nama !== '') {
      dispatch({ type: 'SET_USER', payload: nama });
      navigation.replace('MainApp');
    } else {
      setError(true);
    }
  };

  return (
    <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={styles.container}>
        <Text style={styles.title}>INFORMATION of COVID-19</Text>
        <Image source={Image3} style={{ width: 289, height: 146 }} />
        <View style={styles.wrappForm}>
          <Text style={error ? styles.warningText : styles.hiddenText}>
            Harap Mengisi Nama Anda !!
          </Text>

          <View style={styles.wrappInput}>
            <FontAwesome5 name="user-circle" size={24} color="#3F3D56" />
            <TextInput
              style={styles.input}
              placeholder="Nama anda ..."
              value={nama}
              onChangeText={(value) => setNama(value)}
            />
          </View>
          <View style={{ height: 22 }} />
          <View style={styles.wrappInput}>
            <MaterialIcons name="lock-outline" size={24} color="#3F3D56" />
            <TextInput
              placeholder="Password anda ..."
              secureTextEntry={true}
              style={styles.input}
            />
          </View>
        </View>
        <TouchableOpacity onPress={moveToHome}>
          <View style={styles.btn}>
            <Text style={styles.textBtn}>LOGIN</Text>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: Constant.statusBarHeight,
    alignItems: 'center',
  },
  title: {
    marginTop: 36,
    fontSize: 24,
    textAlign: 'center',
    width: 190,
    fontWeight: 'bold',
    color: '#3F3D56',
    marginBottom: 40,
  },
  wrappForm: {
    width: 236,
    height: 130,
    backgroundColor: '#E5E5E5',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 18,
    paddingBottom: 10,
  },
  wrappInput: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'white',
    width: 162,
    height: 30,
    borderRadius: 5,
    marginLeft: 7,
    paddingHorizontal: 3,
  },
  btn: {
    backgroundColor: '#6C63FF',
    marginTop: 30,
    width: 109,
    height: 26,
    borderRadius: 15,
    justifyContent: 'center',
  },
  textBtn: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 13,
  },
  warningText: {
    fontSize: 11,
    alignSelf: 'center',
    marginBottom: 5,
    color: 'red',
  },
  hiddenText: {
    color: 'transparent',
    fontSize: 11,
    alignSelf: 'center',
  },
});
