import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import Constant from 'expo-constants';
import UserPhoto from '../../assets/image/user-photo.png';
import DataIndo from '../../components/DataIndo';
import News from '../../components/News';
import Axios from 'axios';
import { useSelector } from 'react-redux';

const Home = ({ navigation }) => {
  const [dataIndo, setDataIndo] = useState([]);
  const [dataNews, setDataNews] = useState([]);

  useEffect(() => {
    getDataIndo();
    getDataNews();
  }, []);

  const currentUser = useSelector((state) => {
    return state.setUser;
  });
  // console.log(currentUser);

  const getDataIndo = () => {
    Axios.get('https://api.kawalcorona.com/indonesia/')
      .then((res) => {
        // console.log(res.data[0]);
        setDataIndo(res.data[0]);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getDataNews = () => {
    Axios.get(
      'https://newsapi.org/v2/top-headlines?category=health&country=id&pageSize=5&apiKey=bb411f6216414064abf389ee07572a3d'
    )
      .then((res) => {
        // console.log(res.data.articles);
        setDataNews(res.data.articles);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // console.log(dataIndo);
  return (
    <View style={styles.container}>
      <View style={styles.wrappHeader}>
        <Image source={UserPhoto} style={{ width: 64, height: 64 }} />
        <View style={styles.wrappDesc}>
          <Text style={styles.email}>Hi,</Text>
          <Text style={styles.nama}>{currentUser}</Text>
        </View>
      </View>
      <Text style={styles.title}>Data Indonesia</Text>

      <DataIndo
        positif={dataIndo.positif}
        sembuh={dataIndo.sembuh}
        meninggal={dataIndo.meninggal}
      />

      <Text style={styles.title}>News Headline</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        {dataNews.map((item, index) => {
          return (
            <News
              key={index}
              title={item.title}
              source={item.source.name}
              photo={item.urlToImage}
              onPressed={() =>
                navigation.navigate('DetailNews', { url: item.url })
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constant.statusBarHeight,
    backgroundColor: 'white',
  },
  wrappHeader: {
    flexDirection: 'row',
    marginLeft: 30,
    marginTop: 20,
    alignItems: 'center',
  },
  wrappDesc: {
    marginLeft: 30,
  },
  nama: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#3F3D56',
  },
  email: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#C4C4C4',
  },
  title: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#3F3D56',
    marginVertical: 35,
    marginLeft: 30,
  },
});
