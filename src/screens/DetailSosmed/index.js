import Constant from 'expo-constants';
import React from 'react';
import { WebView } from 'react-native-webview';

const DetailSosmed = ({ route }) => {
  const { linkIg } = route.params;
  // console.log('isi params:', linkIg)

  return (
    <WebView
      domStorageEnabled={true}
      javaScriptEnabled={true}
      source={{ uri: linkIg }}
      style={{ marginTop: Constant.statusBarHeight }}
    />
  );
};

export default DetailSosmed;
