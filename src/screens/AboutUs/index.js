import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import Team from '../../assets/image/Team.png';
import Muhammad from '../../components/Muhammad';

const AboutUs = ({ navigation }) => {
  const sosmed1 = {
    ig: 'https://www.instagram.com/evi.silaban__/?hl=id',
    fb: 'https://web.facebook.com/profile.php?id=100008696475012',
    tw: 'https://twitter.com/EviRosalinaSil1',
  };
  // console.log(sosmed1.ig);
  const sosmed2 = {
    ig1: 'https://www.instagram.com/mpadri_/',
    fb1: 'https://web.facebook.com/muhaPadri',
    tw1: 'https://twitter.com/mpadri12',
  };
  // console.log(sosmed2.ig)
  return (
    <View style={styles.container}>
      <View style={styles.About}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 30,
            textAlign: 'center',
            marginLeft: 10,
            color: '#3F3D56',
          }}
        >
          About Us
        </Text>
        <Image
          source={Team}
          style={{ width: 120, height: 120, marginLeft: 10 }}
        />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Muhammad
          onPressed={() =>
            navigation.navigate('DetailSosmed', { linkIg: sosmed1.ig })
          }
          pressFb={() =>
            navigation.navigate('DetailSosmed', { linkIg: sosmed1.fb })
          }
          pressTw={() =>
            navigation.navigate('DetailSosmed', { linkIg: sosmed1.tw })
          }
          pressIg={() =>
            navigation.navigate('DetailSosmed', { linkIg: sosmed2.ig1 })
          }
          pressFb1={() =>
            navigation.navigate('DetailSosmed', { linkIg: sosmed2.fb1 })
          }
          pressTw1={() =>
            navigation.navigate('DetailSosmed', { linkIg: sosmed2.tw1 })
          }
        />
      </ScrollView>
    </View>
  );
};

export default AboutUs;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  About: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 190,
    paddingHorizontal: 15,
  },
});
