import React, { useState, useEffect } from 'react';
import Constant from 'expo-constants';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import Image2 from '../../assets/image/img2.png';
import Card from '../../components/Card';
import Axios from 'axios';

const Region = () => {
  const [dataProvinsi, setDataProvinsi] = useState([]);

  useEffect(() => {
    getDataProvinsi();
  }, []);

  const getDataProvinsi = () => {
    Axios.get('https://api.kawalcorona.com/indonesia/provinsi')
      .then((res) => {
        // console.log(res.data);
        setDataProvinsi(res.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Data Provinsi</Text>
      <View style={styles.wrappGambar}>
        <Image source={Image2} style={{ width: 200, height: 100 }} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {dataProvinsi.map((item) => {
          // console.log(item.attributes.Provinsi);
          return (
            <Card
              key={item.attributes.FID}
              positif={item.attributes.Kasus_Posi}
              sembuh={item.attributes.Kasus_Semb}
              meninggal={item.attributes.Kasus_Meni}
              provinsi={item.attributes.Provinsi}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default Region;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constant.statusBarHeight,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#3F3D56',
    marginLeft: 30,
    marginTop: 40,
  },
  wrappGambar: {
    alignItems: 'center',
    marginVertical: 30,
  },
});
