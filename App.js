import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Route from './src/routes';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { NamaReducer } from './src/redux/reducer/NamaReducer';

const rootReducer = combineReducers({
  setUser: NamaReducer,
});

const store = createStore(rootReducer);

export const RootNavigation = () => {
  return (
    <View style={styles.container}>
      <NavigationContainer>
        <Route />
      </NavigationContainer>
    </View>
  );
};

export default function App() {
  return (
    <Provider store={store}>
      <RootNavigation />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
